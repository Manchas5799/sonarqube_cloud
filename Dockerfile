# Use the official SonarQube Developer Edition image as the base image
FROM sonarqube:developer

# Expose the default SonarQube web port
EXPOSE 9000
